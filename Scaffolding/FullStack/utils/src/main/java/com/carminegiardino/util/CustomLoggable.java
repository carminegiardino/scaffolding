package com.carminegiardino.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface CustomLoggable {
    default Logger log() {
        return LoggerFactory.getLogger(getClass().getName());
    }
}
